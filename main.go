package main

import (
	"bufio"
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"os"
)

func main() {

	in := bufio.NewReader(os.Stdin)

	ch := make(chan string)
	quit := make(chan struct{})

	go page(ch, quit)

	go readInput(in, ch)

	select {
	case <-quit:
	}
}

func readInput(in io.Reader, ch chan<- string) {
	scanner := bufio.NewScanner(in)
	for scanner.Scan() {
		ch <- scanner.Text()
	}
	close(ch)
}

func page(source <-chan string, quit chan<- struct{}) {
	tty := os.NewFile(2, "<tty>")
	width, height, err := terminal.GetSize(2)
	if err != nil {
		fmt.Println(err)
	}
	_ = width

	line := []string{}
	pageLength := height - 1

	// First line is 0
	// displayTop and Limit are the base and limit of
	// the currently displayed lines.
	displayTop := 0
	displayLimit := 0
	// Initialise to largest number
	sourceLength := int(^uint(0) >> 1)
	maxTop := sourceLength
	// (Line number of) top line about to be displayed.
	newTop := 0

	for {
		// One more than index of last line to be displayed
		newLimit := newTop + pageLength
		newLimit = min(newLimit, sourceLength)

		// There are 3 modes of new display:
		// Simple: the new window limit is greater than
		// the current limit. Lines that are not yet
		// displayed are printed (fetched if necessary).
		// Clear: the new window limit precedes the
		// current top (no overlap). The screen is
		// cleared and lines printed from top to bottom.
		// Reverse: the new window limit is between
		// the current top and limit. The screen is
		// scrolled down and lines printed at the top.

		repositionPrompt := false

		// First we prepare the display,
		// then we print lines from i to j
		i := newTop
		j := newLimit

		if newLimit <= displayTop {
			clearDisplay()
		} else if displayTop < newLimit &&
			newLimit < displayLimit {
			// Reverse
			n := displayTop - newTop
			scrollDown(n)
			home()
			j = displayTop
			repositionPrompt = true
		} else {
			// Simple
			i = max(i, displayLimit)
		}

		for i < j {
			if i >= len(line) {
				l, ok := <-source
				if ok {
					line = append(line, l)
				} else {
					sourceLength = len(line)
					maxTop = max(newLimit-pageLength, 0)
					newLimit = sourceLength
					newTop = max(newTop, maxTop)
					break
				}
			}

			fmt.Println(line[i])
			i += 1
		}

		displayTop = newTop
		displayLimit = newLimit

		if repositionPrompt {
			down(pageLength)
		}

		fmt.Printf("%d:%d", newTop, newLimit)
		cmd, gotNumber, number := prompt(tty)
		switch cmd {
		case "b", "\002":
			if !gotNumber {
				number = pageLength
			}
			newTop = displayTop - number
		case "f", "\006", " ":
			if !gotNumber {
				number = pageLength
			}
			newTop = displayTop + number
		case "j", "l", "\n", "\r":
			if !gotNumber {
				number = 1
			}
			newTop = displayTop + number
		case "k":
			if !gotNumber {
				number = 1
			}
			newTop = displayTop - number
		case "q":
			close(quit)
			select {}
		default:
		}
		// newTop can't go below 0...
		newTop = max(newTop, 0)
		// nor can it go larger than maxTop.
		newTop = min(newTop, maxTop)
	}

}

func max(x, y int) int {
	if y < x {
		return x
	}
	return y
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// Issue prompt on tty and read keypress
func prompt(tty *os.File) (string, bool, int) {
	ttyRestoreState, ttyError := terminal.MakeRaw(int(tty.Fd()))
	if ttyError == nil {
		defer terminal.Restore(int(tty.Fd()), ttyRestoreState)
	}
	sgrNegative()
	fmt.Print("Tap Space")
	sgrDefault()

	buffer := make([]byte, 1)
	collected := []byte{}
	gotNumber := false
	num := 0

	for {
		n, _ := tty.Read(buffer)
		if n == 0 {
			break
		}
		c := buffer[0]
		if '0' <= c && c <= '9' {
			gotNumber = true
			num = num*10 + int(c-'0')
		} else {
			collected = append(collected, c)
			break
		}
	}

	fmt.Print("\r")
	clearLine()
	return string(collected), gotNumber, num
}

// ANSI CUP ED Home and Erase in Display
func clearDisplay() {
	home()
	fmt.Print("\033[J")
}

// ANSI EL Erase in Line function
func clearLine() {
	fmt.Print("\033[2K")
}

// ANSI CUD
func down(n int) {
	fmt.Printf("\033[%dB", n)
}

// ANSI CUP
func home() {
	fmt.Print("\033[H")
}

// ANSI SD
func scrollDown(n int) {
	if n > 0 {
		fmt.Printf("\033[%dT", n)
	}
}

// ANSI SGR Select Graphic Rendition Negative (reverse video)
func sgrNegative() {
	fmt.Print("\033[7m")
}

// ANSI SGR Select Graphic Rendition Default
func sgrDefault() {
	fmt.Print("\033[m")
}
